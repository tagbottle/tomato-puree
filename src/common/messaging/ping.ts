/*
   user's chat ping
 */

export interface IPingNotification {
  data: {
    encodedTag: string;

    pingUsername: string;
    pingDisplayName: string;

    iconUrl: string;
  };
}

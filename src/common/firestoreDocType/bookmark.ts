export interface IBookmark {
  tags?: { [key: string]: boolean };
}

export interface IBookmarkDocument extends IBookmark {
  tags: { [key: string]: boolean };
}

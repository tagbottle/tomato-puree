export interface IUsername {
  user?: string;
}

export interface IUsernameDocument extends IUsername {
  user: string;
}

import { Currency, Reference, Time } from "./_const";

export type StampOrderStatus = "ok" | "failed" | "pending" | "working";

export interface IStampOrder {
  user?: Reference;
  paymentToken?: string;

  stampSet?: Reference;
  amount?: number;
  price?: string;
  currency?: Currency;
  createdAt?: Time;

  status?: StampOrderStatus;
  finishedAt?: Time | null;
}

export interface IStampOrderDocument extends IStampOrder {
  user: Reference;
  paymentToken: string;
  stampSet: Reference;
  amount: number;
  price: string;
  currency: Currency;
  createdAt: Time;

  status: StampOrderStatus;
  finishedAt?: Time | null;
}

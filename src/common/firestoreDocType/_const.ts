export type DocumentReference = any;
export type FieldValue = any;

export interface ITimestamp {
  seconds: number;
  nanoseconds: number;
}

export type Reference = DocumentReference | string;
export type Time = FieldValue | ITimestamp;

export type Currency = "JPY";

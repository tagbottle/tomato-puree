import { Reference, Time } from "./_const";

export interface IMessageEditLog {
  bottle?: Reference;
  text?: string;
  timestamp?: Time;
  user?: Reference;
}

export interface IMessageEditLogDocument extends IMessageEditLog {
  bottle: Reference;
  text: string;
  timestamp: Time;
  user: Reference;
}

import { Reference, Time } from "../../_const";

export interface IStampStockStampSet {
  amount?: number;
  favorite?: boolean;

  lastUsedAt?: Time | null;
  lastBoughtAt?: Time;
}

export interface IStampStockStampSetDocument extends IStampStockStampSet {
  amount: number;
  favorite: boolean;

  lastUsedAt: Time | null;
  lastBoughtAt: Time;
}

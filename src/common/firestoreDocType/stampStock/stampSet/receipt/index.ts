import { Reference, Time } from "../../../_const";

export interface IStampStockReceipt {
  amount?: number;
  boughtAt?: Time;
  logs?: IStampStockReceiptItem[];
}

export interface IStampStockReceiptDocument extends IStampStockReceipt {
  amount: number;
  boughtAt: Time;
  logs: IStampStockReceiptItemDocument[];
}

/*
  item
*/

export interface IStampStockReceiptItem {
  message?: Reference;
  stampItemId?: string;
}

export interface IStampStockReceiptItemDocument extends IStampStockReceiptItem {
  message: Reference;
  stampItemId: string;
}

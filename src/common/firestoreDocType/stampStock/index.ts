import { Reference, Time } from "../_const";

export interface IStampStock {
  favorites?: IStampStockFavCache[];
  latestUsed?: IStampStockUsedCache[];
  latestBought?: IStampStockBoughtCache[];
}

export interface IStampStockDocument extends IStampStock {
  favorites: IStampStockFavCacheDocument[];
  latestUsed: IStampStockUsedCacheDocument[];
  latestBought: IStampStockBoughtCacheDocument[];
}

/*
  fav
*/

export interface IStampStockFavCache {
  stampSet?: Reference;
  amount?: number;
}

export interface IStampStockFavCacheDocument extends IStampStockFavCache {
  stampSet: Reference;
  amount: number;
}

/*
  use history
*/

export interface IStampStockUsedCache {
  stampSet?: Reference;
  amount?: number;
}

export interface IStampStockUsedCacheDocument extends IStampStockUsedCache {
  stampSet: Reference;
  amount: number;
}

/*
  buy history
*/

export interface IStampStockBoughtCache {
  stampSet?: Reference;
  amount?: number;
}

export interface IStampStockBoughtCacheDocument extends IStampStockBoughtCache {
  stampSet: Reference;
  amount: number;
}

import { Reference, Time } from "../../_const";

export interface IMessageStampSet {
  stampItems?: { [key: string]: IMessageStampSetItem };
}
export interface IMessageStampSetDocument extends IMessageStampSet {
  stampItems: { [key: string]: IMessageStampSetItem };
}

/*
  item of stamp set
 */

export interface IMessageStampSetItem {
  amount?: number;
  lastUsed?: Time;
}

export interface IMessageStampSetItemDocument extends IMessageStampSetItem {
  amount: number;
  lastUsed: Time;
}

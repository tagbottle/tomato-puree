export interface IMessageStampRequest {
  stampItemId?: string;
}

export interface IMessageStampRequestDocument extends IMessageStampRequest {
  stampItemId: string;
}

import { Reference, Time } from "../_const";

export interface IMessage {
  bottle?: Reference;
  index?: number;
  text?: string;
  timestamp?: Time;
  stamps?: IStampSummary[];
  themes?: string[];
  user?: Reference;
  citations?: string[];
}

export interface IMessageDocument extends IMessage {
  bottle: Reference;
  index: number;
  text: string;
  timestamp: Time;
  stamps: IStampSummaryDocument[];
  themes: string[];
  user: Reference;
  citations: string[];
}

/*
  stamp summary
 */
export interface IStampSummary {
  stampSet?: Reference;
  stampItemId?: string;
  amount?: number;
}

export interface IStampSummaryDocument extends IStampSummary {
  stampSet: Reference;
  stampItemId: string;
  amount: number;
}

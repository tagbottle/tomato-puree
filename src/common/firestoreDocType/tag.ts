import { Reference } from "./_const";

export interface ITag {
  currentBottle?: Reference;
  currentIndex?: number;
  parent?: string | null;
  bottles?: { [key: number]: Reference };
}

export interface ITagDocument extends ITag {
  currentBottle: Reference;
  currentIndex: number;
  parent: string | null;
  bottles: { [key: number]: Reference };
}

import { Currency, Reference, Time } from "./_const";

export type StampReceiptStatus = "ok";
export interface IStampReceipt {
  user?: Reference;
  stampSet?: Reference;
  order?: Reference;

  transactionId?: string;

  status?: StampReceiptStatus;
  createdAt?: Time;
}

export interface IStampReceiptDocument extends IStampReceipt {
  user: Reference;
  stampSet: Reference;
  order: Reference;

  transactionId?: string;

  status: StampReceiptStatus;
  createdAt: Time;
}

import { Time } from "./_const";

export interface IAdmin {
  createdAt?: Time;
}

export interface IAdminDocument {
  createdAt: Time;
}

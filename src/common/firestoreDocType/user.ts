export interface IUser {
  displayName?: string;
  photoUrl?: string;
  username?: string;
}

export interface IUserDocument extends IUser {
  displayName: string;
  photoUrl: string;
  username?: string;
}

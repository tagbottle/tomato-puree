import { Reference } from "./_const";

export interface IBottle {
  index?: number;
  tag?: Reference;
  finished?: true;
}

export interface IBottleDocument extends IBottle {
  index: number;
  tag: Reference;
}

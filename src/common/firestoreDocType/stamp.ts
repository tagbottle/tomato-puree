import { Currency, Reference, Time } from "./_const";

export type StampStatus = "ok" | "pending" | "failed";

export interface IStamp {
  user?: Reference;
  stampSet?: Reference;
  stampItemId?: string;

  message?: Reference;
  status?: StampStatus;
}

export interface IStampDocument extends IStamp {
  user: Reference;
  stampSet: Reference;
  stampItemId: string;

  message: Reference;
  status: StampStatus;
}

export interface IUserDevice {
  devices?: IUserDeviceInfo[];
}

export interface IUserDeviceDocument extends IUserDevice {
  devices: IUserDeviceInfoDocument[];
}

/*
  info of each device
 */

export interface IUserDeviceInfo {
  name?: string;
  token?: string;
  active?: boolean;
}

export interface IUserDeviceInfoDocument extends IUserDeviceInfo {
  name: string;
  token: string;
  active: boolean;
}

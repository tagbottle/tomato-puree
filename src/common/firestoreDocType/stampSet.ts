export interface IStampSet {
  vendor?: string;
  title?: string;
  items?: IStampSetItem[];
}
export interface IStampSetDocument extends IStampSet {
  vendor: string;
  title: string;
  items: IStampSetItemDocument[];
}

/*
  item of stamp set
 */

export interface IStampSetItem {
  id?: string;
  index?: number;
  name?: string;
  url?: string;
}

export interface IStampSetItemDocument extends IStampSetItem {
  id: string;
  index: number;
  name: string;
  url: string;
}

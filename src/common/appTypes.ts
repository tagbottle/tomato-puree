/*
   fcm notification
 */

export interface INewMessageNotification {
  data: {
    bottleId: string;
    messageId: string;

    index: string;
    encodedTag: string;
    text: string;

    userId: string;
    username: string;
    displayName: string;
    photoUrl: string;
  };
  topic: string;
}

export interface INewMessageNotificationData {
  bottleId: string;
  messageId: string;

  index: string;
  encodedTag: string;
  text: string;

  userId: string;
  username: string;
  displayName: string;
  photoUrl: string;
}

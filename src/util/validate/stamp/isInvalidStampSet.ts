import validate from "validate.js";

export function isInvalidStampSet(target: object): boolean {
  // presence
  const invalidArguments = validate(target, {
    items: {
      presence: true
    },
    title: {
      presence: true
    },
    vendor: {
      presence: true
    }
  });
  if (invalidArguments) {
    return true;
  }

  // type check
  const { vendor, title, items } = target as {
    vendor: any;
    title: any;
    items: any;
  };
  if (
    !validate.isString(vendor) ||
    !validate.isString(title) ||
    !validate.isArray(items)
  ) {
    return true;
  }

  // stamps array
  for (const item of items) {
    // presence
    const invalidStamp = validate(item, {
      id: {
        presence: true
      },
      index: {
        numericality: true,
        presence: true
      },
      name: {
        presence: true
      },
      url: {
        presence: true,
        url: true
      }
    });
    if (invalidStamp) {
      return true;
    }

    // type check
    const { id, url } = item;
    if (!validate.isString(id) || !validate.isString(url)) {
      return true;
    }
  }

  // all checks passed: not invalid
  return false;
}

export default isInvalidStampSet;

export type ThemesError = "NOT_ARRAY" | "EMPTY" | "INVALID_THEME_INCLUDED";

export const isInvalidThemes = (themes: string[]): ThemesError | null => {
  if (!themes || !Array.isArray(themes)) {
    return "NOT_ARRAY";
  }

  if (themes.length === 0) {
    return "EMPTY";
  }

  for (const theme of themes) {
    if (!theme || typeof theme !== "string") {
      return "INVALID_THEME_INCLUDED";
    }
  }

  return null;
};

export default isInvalidThemes;

export type TextError = "EMPTY";

export function isInvalidText(text: string): TextError | null {
  if (!text) {
    return "EMPTY";
  }

  return null;
}

export default isInvalidText;

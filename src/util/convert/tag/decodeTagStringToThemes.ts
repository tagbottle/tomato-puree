export const decodeTagStringToThemes = (
  encodedTag: string
): string[] | null => {
  if (!encodedTag || !encodedTag.length) {
    return null;
  }
  const decodeKey = (key: string) =>
    decodeURIComponent(key.replace("	%2E", "."));
  return decodeKey(encodedTag)
    .split("/")
    .map(decodeKey);
};

export default decodeTagStringToThemes;

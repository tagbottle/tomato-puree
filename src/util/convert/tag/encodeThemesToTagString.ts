export const encodeThemesToTagString = (themes: string[]): string | null => {
  if (!themes || !Array.isArray(themes) || !themes.length) {
    return null;
  }
  for (const theme of themes) {
    if (typeof theme !== "string") {
      return null;
    }
  }
  const encodeKey = (key: string) =>
    encodeURIComponent(key).replace(".", "	%2E");
  return encodeKey(themes.map(encodeKey).join("/"));
};

export default encodeThemesToTagString;

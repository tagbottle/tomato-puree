import themesForTag from "./themesForTag";

export interface INestedTag {
  theme: string;
  themes: string[];
  active: boolean;
  children: INestedTag[];
}

export const nestedList = (tags: string[]): INestedTag[] => {
  const result: INestedTag[] = [];

  for (const tag of tags) {
    const themes = themesForTag(tag) || [];
    const firstTheme = themes.shift();
    if (!firstTheme) {
      continue;
    }

    let currentTag: INestedTag;
    const existingFirstTag = result.find(t => t.theme === firstTheme);
    if (existingFirstTag) {
      currentTag = existingFirstTag;
    } else {
      currentTag = {
        active: false,
        children: [],
        theme: firstTheme,
        themes: [firstTheme]
      };
      result.push(currentTag);
    }

    for (const theme of themes) {
      const existingChild = currentTag.children.find(t => t.theme === theme);
      if (existingChild) {
        currentTag = existingChild;
      } else {
        const newChildTag = {
          active: false,
          children: [],
          theme,
          themes: currentTag.themes.concat(theme)
        };
        currentTag.children.push(newChildTag);
        currentTag = newChildTag;
      }
    }
    currentTag.active = true;
  }

  return result;
};

export default nestedList;

/**
 *  convert tag to themes array
 */
export const themesForTag = (tag: string): string[] | null => {
  if (!tag) {
    return null;
  }
  const themes = tag.split("/").filter(theme => !!theme);
  if (themes.length === 0) {
    return null;
  }
  return themes;
};

export default themesForTag;

import encode from "./encode";

export const tagSubscribeToken = (themes: string[]): string =>
  `/topics/${encode(themes.map(encode).join("/"))}`;
export default tagSubscribeToken;

export const encode = (key: string): string => {
  return encodeURIComponent(key).replace(
    /[!'()*]/g,
    c => "%" + c.charCodeAt(0).toString(16)
  );
};

export default encode;

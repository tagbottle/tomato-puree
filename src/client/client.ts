import firestore from "@firebase/firestore-types";

export type Db = firestore.FirebaseFirestore;
export type DocumentReference = firestore.DocumentReference;
export type Transaction = firestore.Transaction;
export type FieldValue = firestore.FieldValue;
export type SetOptions = firestore.SetOptions;
export type UpdateData = firestore.UpdateData;
export type Query = firestore.Query;
export type QuerySnapshot = firestore.QuerySnapshot;

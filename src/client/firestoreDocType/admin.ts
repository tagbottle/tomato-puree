import { CommonType, Time } from "./_util";

export interface IAdmin extends CommonType.IAdmin {
  createdAt?: Time;
}

export interface IAdminDocument extends CommonType.IAdminDocument {
  createdAt: Time;
}

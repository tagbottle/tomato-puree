import { CommonType } from "./_util";
export type IStampSet = CommonType.IStampSet;
export type IStampSetDocument = CommonType.IStampSetDocument;
export type IStampSetItem = CommonType.IStampSetItem;
export type IStampSetItemDocument = CommonType.IStampSetItemDocument;

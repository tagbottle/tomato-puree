import { CommonType, DocumentReference, Reference, Time } from "./_util";

export interface IStamp extends CommonType.IStamp {
  user?: Reference;
  stampSet?: Reference;
  message?: Reference;
}

export interface IStampDocument extends CommonType.IStampDocument {
  user: DocumentReference;
  stampSet: DocumentReference;
  message: DocumentReference;
}

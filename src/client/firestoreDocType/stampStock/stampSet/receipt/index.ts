import { CommonType, DocumentReference, Reference, Time } from "../../../_util";

export interface IStampStockReceipt extends CommonType.IStampStockReceipt {
  boughtAt?: Time;
  logs?: IStampStockReceiptItem[];
}

export interface IStampStockReceiptDocument
  extends CommonType.IStampStockReceiptDocument {
  boughtAt: Time;
  logs: IStampStockReceiptItemDocument[];
}

/*
  item
*/

export interface IStampStockReceiptItem
  extends CommonType.IStampStockReceiptItem {
  message?: Reference;
}

export interface IStampStockReceiptItemDocument
  extends CommonType.IStampStockReceiptItemDocument {
  message: DocumentReference;
}

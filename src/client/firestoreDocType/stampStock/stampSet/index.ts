import { CommonType, DocumentReference, Reference, Time } from "../../_util";

export interface IStampStockStampSet extends CommonType.IStampStockStampSet {
  lastUsedAt?: Time | null;
  lastBoughtAt?: Time;
}

export interface IStampStockStampSetDocument
  extends CommonType.IStampStockStampSetDocument {
  lastUsedAt: Time | null;
  lastBoughtAt: Time;
}

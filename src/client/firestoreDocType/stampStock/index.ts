import { CommonType, DocumentReference, Reference, Time } from "../_util";

export interface IStampStock extends CommonType.IStampStock {
  favorites?: IStampStockFavCache[];
  latestUsed?: IStampStockUsedCache[];
  latestBought?: IStampStockBoughtCache[];
}

export interface IStampStockDocument extends CommonType.IStampStockDocument {
  favorites: IStampStockFavCacheDocument[];
  latestUsed: IStampStockUsedCacheDocument[];
  latestBought: IStampStockBoughtCacheDocument[];
}

/*
  fav
*/

export interface IStampStockFavCache extends CommonType.IStampStockFavCache {
  stampSet?: Reference;
}

export interface IStampStockFavCacheDocument
  extends CommonType.IStampStockFavCacheDocument {
  stampSet: DocumentReference;
}

/*
  use history
*/

export interface IStampStockUsedCache extends CommonType.IStampStockUsedCache {
  stampSet?: Reference;
}

export interface IStampStockUsedCacheDocument
  extends CommonType.IStampStockUsedCacheDocument {
  stampSet: DocumentReference;
}

/*
  buy history
*/

export interface IStampStockBoughtCache
  extends CommonType.IStampStockBoughtCache {
  stampSet?: Reference;
}

export interface IStampStockBoughtCacheDocument
  extends CommonType.IStampStockBoughtCacheDocument {
  stampSet: DocumentReference;
}

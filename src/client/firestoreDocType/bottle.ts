import { CommonType, DocumentReference, Reference } from "./_util";

export interface IBottle extends CommonType.IBottle {
  tag?: Reference;
}

export interface IBottleDocument extends CommonType.IBottleDocument {
  tag: DocumentReference;
}

import { CommonType, DocumentReference, Reference, Time } from "./_util";

export type StampOrderStatus = "ok" | "pending";

export interface IStampOrder extends CommonType.IStampOrder {
  user?: Reference;
  stampSet?: Reference;
  createdAt?: Time;
  finishedAt?: Time | null;
}

export interface IStampOrderDocument extends CommonType.IStampOrderDocument {
  user: DocumentReference;
  stampSet: DocumentReference;
  createdAt: Time;
  finishedAt?: Time | null;
}

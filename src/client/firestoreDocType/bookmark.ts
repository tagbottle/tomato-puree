import { CommonType } from "./_util";

export type IBookmark = CommonType.IBookmark;
export type IBookmarkDocument = CommonType.IBookmarkDocument;

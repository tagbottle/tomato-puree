import { CommonType } from "./_util";

export type IUserDevice = CommonType.IUserDevice;
export type IUserDeviceDocument = CommonType.IUserDeviceDocument;
export type IUserDeviceInfo = CommonType.IUserDeviceInfo;
export type IUserDeviceInfoDocument = CommonType.IUserDeviceInfoDocument;

import * as CommonType from "../../common";
import { DocumentReference, FieldValue } from "../client";

type Reference = DocumentReference | string;
type Time = FieldValue | CommonType.ITimestamp | Date;

export { CommonType, DocumentReference, FieldValue, Reference, Time };

import { CommonType, DocumentReference, Reference, Time } from "./_util";

export interface IStampReceipt extends CommonType.IStampReceipt {
  user?: Reference;
  stampSet?: Reference;
  order?: Reference;
  createdAt?: Time;
}

export interface IStampReceiptDocument
  extends CommonType.IStampReceiptDocument {
  user: DocumentReference;
  stampSet: DocumentReference;
  order: DocumentReference;
  createdAt: Time;
}

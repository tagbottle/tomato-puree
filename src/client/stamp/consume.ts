import firestore from "@firebase/firestore-types";
import { either } from "fp-ts";
import validate from "validate.js";

import * as resultType from "../../type/result";
import { Db } from "../client";
import { IStampDocument } from "../firestoreDocType";
import {
  MessageRef,
  RefGenerator,
  StampRef,
  StampSetRef,
  UserRef
} from "../ref";

export interface IProps {
  userRef: UserRef;
  messageRef: MessageRef;
  stampSetRef: StampSetRef;
  stampItemId: string;
}

export interface ISuccessPayload {
  stampRef: StampRef;
}

type Result = resultType.StandardResult<"invalid-argument", ISuccessPayload>;

function validateProps(props: IProps): string | null {
  if (!validate.isObject(props)) {
    return "props is not object";
  }
  const { userRef, messageRef, stampSetRef, stampItemId } = props;

  // userRef
  if (!userRef) {
    return "userRef is blank";
  }

  // messageRef
  if (!messageRef) {
    return "messageRef is blank";
  }

  // stampSetRef
  if (!stampSetRef) {
    return "stampSetRef is blank";
  }

  // stampItemId
  if (!validate.isString(stampItemId)) {
    return "stampItemId is not string";
  }

  return null;
}

export const orderStamp = async (db: Db, props: IProps): Promise<Result> => {
  // validate
  const errorMessage = validateProps(props);
  if (errorMessage) {
    return resultType.leftError("invalid-argument", errorMessage);
  }
  const { userRef, messageRef, stampSetRef, stampItemId } = props;

  // util setup
  const refGenerator = new RefGenerator(db);

  // create new doc
  const stampRef = refGenerator.newStamp();
  const stampDoc: IStampDocument = {
    message: messageRef.ref,
    stampItemId,
    stampSet: stampSetRef.ref,
    status: "pending",
    user: userRef.ref
  };
  await stampRef.set(stampDoc);

  return resultType.right({ stampRef });
};

export default orderStamp;

import firestore from "@firebase/firestore-types";
import { either } from "fp-ts";
import validate from "validate.js";

import * as resultType from "../../type/result";
import { Db, FieldValue } from "../client";
import { CommonType, IStampOrderDocument } from "../firestoreDocType";
import { RefGenerator, StampOrderRef, StampSetRef, UserRef } from "../ref";

export interface IProps {
  userRef: UserRef;
  paymentToken: string;
  stampSetRef: StampSetRef;
  amount: 100;
  price: "0";
  currency: CommonType.Currency;
  timestamp: FieldValue;
}

export interface ISuccessPayload {
  stampOrderRef: StampOrderRef;
}

type Result = resultType.StandardResult<"invalid-argument", ISuccessPayload>;

function validateProps(props: IProps): string | null {
  const { userRef, stampSetRef, currency, amount, paymentToken, price } = props;
  if (!validate.isNumber(amount)) {
    return "amount is not number";
  }
  if (!validate.isString(paymentToken)) {
    return "paymentToken is not string";
  }
  if (!validate.isString(price)) {
    return "price is not string";
  }
  return null;
}

export const orderStamp = async (db: Db, props: IProps): Promise<Result> => {
  // validate
  const errorMessage = validateProps(props);
  if (errorMessage) {
    return resultType.leftError("invalid-argument", errorMessage);
  }

  // util setup
  const {
    userRef,
    stampSetRef,
    currency,
    amount,
    paymentToken,
    price,
    timestamp
  } = props;
  const refGenerator = new RefGenerator(db);

  // create new doc
  const stampOrderRef = refGenerator.newStampOrder();
  const stampOrderDoc: IStampOrderDocument = {
    amount,
    createdAt: timestamp,
    currency,
    paymentToken,
    price,
    stampSet: stampSetRef.ref,
    status: "pending",
    user: userRef.ref
  };
  await stampOrderRef.set(stampOrderDoc);

  return resultType.right({ stampOrderRef });
};

export default orderStamp;

import {
  DocumentReference,
  SetOptions,
  Transaction,
  UpdateData
} from "../client";
import * as DocTypes from "../firestoreDocType";

export class PureeRef<AvailableProps, DocInterface> {
  constructor(public ref: DocumentReference) {}

  /*
    set
   */

  public async set(data: AvailableProps, options: SetOptions = {}) {
    return this.ref.set(data, options);
  }

  /*
    update
   */

  public async update(data: AvailableProps) {
    return this.ref.update(data);
  }

  /*
    get data
   */

  public async data(): Promise<DocInterface | undefined> {
    // get
    const snapshot = await this.ref.get();
    if (!snapshot.exists) {
      return undefined;
    }

    // cast
    const data = snapshot.data();
    return data as DocInterface;
  }
}

export type AdminRef = PureeRef<DocTypes.IAdmin, DocTypes.IAdminDocument>;
export type BookmarkRef = PureeRef<
  DocTypes.IBookmark,
  DocTypes.IBookmarkDocument
>;
export type BottleRef = PureeRef<DocTypes.IBottle, DocTypes.IBottleDocument>;

export type MessageRef = PureeRef<DocTypes.IMessage, DocTypes.IMessageDocument>;

export type StampRef = PureeRef<DocTypes.IStamp, DocTypes.IStampDocument>;
export type StampOrderRef = PureeRef<
  DocTypes.IStampOrder,
  DocTypes.IStampOrderDocument
>;
export type StampReceiptRef = PureeRef<
  DocTypes.IStampReceipt,
  DocTypes.IStampReceiptDocument
>;
export type StampSetRef = PureeRef<
  DocTypes.IStampSet,
  DocTypes.IStampSetDocument
>;
export type StampSetItemRef = PureeRef<
  DocTypes.IStampSetItem,
  DocTypes.IStampSetItemDocument
>;

export type StampStockRef = PureeRef<
  DocTypes.IStampStock,
  DocTypes.IStampStockDocument
>;
export type StampStockFavCacheRef = PureeRef<
  DocTypes.IStampStockFavCache,
  DocTypes.IStampStockFavCacheDocument
>;
export type StampStockUsedCacheRef = PureeRef<
  DocTypes.IStampStockUsedCache,
  DocTypes.IStampStockUsedCacheDocument
>;
export type StampStockBoughtCacheRef = PureeRef<
  DocTypes.IStampStockBoughtCache,
  DocTypes.IStampStockBoughtCacheDocument
>;
export type StampStockStampSetRef = PureeRef<
  DocTypes.IStampStockStampSet,
  DocTypes.IStampStockStampSetDocument
>;
export type StampStockReceiptRef = PureeRef<
  DocTypes.IStampStockReceipt,
  DocTypes.IStampStockReceiptDocument
>;
export type StampStockReceiptItemRef = PureeRef<
  DocTypes.IStampStockReceiptItem,
  DocTypes.IStampStockReceiptItemDocument
>;

export type TagRef = PureeRef<DocTypes.ITag, DocTypes.ITagDocument>;
export type UserRef = PureeRef<DocTypes.IUser, DocTypes.IUserDocument>;
export type UsernameRef = PureeRef<
  DocTypes.IUsername,
  DocTypes.IUsernameDocument
>;
export type UserDeviceRef = PureeRef<
  DocTypes.IUserDevice,
  DocTypes.IUserDeviceDocument
>;

export default PureeRef;

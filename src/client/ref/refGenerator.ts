import { StandardError } from "../../type/result";
import encodeThemesToTagString from "../../util/convert/tag/encodeThemesToTagString";
import { Db, Query } from "../client";
import { PureeQuery, StampSetQuery } from "./pureeQuery";
import {
  AdminRef,
  BookmarkRef,
  BottleRef,
  MessageRef,
  PureeRef,
  StampOrderRef,
  StampRef,
  StampSetRef,
  StampStockRef,
  TagRef,
  UserDeviceRef,
  UserRef
} from "./pureeRef";

export class RefGenerator {
  constructor(public db: Db) {}

  /*
    admin
   */

  public admin(userId: string): AdminRef {
    // validation
    if (!userId) {
      throw new StandardError("invalid-argument", "AdminRef requires userId");
    }

    // create ref
    const ref = this.db.collection("admins").doc(userId);
    return new PureeRef(ref);
  }

  /*
    bookmark
   */

  public bookmark(userId: string): BookmarkRef {
    // validation
    if (!userId) {
      throw new StandardError(
        "invalid-argument",
        "BookmarkRef requires userId"
      );
    }

    // create ref
    const ref = this.db.collection("tags").doc(userId);
    return new PureeRef(ref);
  }

  /*
    bottle
  */

  public newBottle(): BottleRef {
    const ref = this.db.collection("bottles").doc();
    return new PureeRef(ref);
  }

  public bottle(bottleId: string): BottleRef {
    const ref = this.db.collection("bottles").doc(bottleId);
    return new PureeRef(ref);
  }

  /*
    message
   */

  public newMessage(): MessageRef {
    const ref = this.db.collection("messages").doc();
    return new PureeRef(ref);
  }

  public message(messageId: string): MessageRef {
    const ref = this.db.collection("messages").doc(messageId);
    return new PureeRef(ref);
  }

  /*
    tag
  */

  public tag(themes: string[]): TagRef {
    // validation
    if (!themes.length) {
      throw new StandardError(
        "invalid-argument",
        "TagRef requires themes.length"
      );
    }
    const encodedTag = encodeThemesToTagString(themes);
    if (!encodedTag) {
      throw new StandardError(
        "invalid-argument",
        "TagRef require correct encodedTag"
      );
    }

    // create ref
    const ref = this.db.collection("tags").doc(encodedTag);
    return new PureeRef(ref);
  }

  /*
    user
  */

  public user(userId: string): UserRef {
    // validation
    if (!userId) {
      throw new StandardError("invalid-argument", "UserRef requires userId");
    }

    // create ref
    const ref = this.db.collection("users").doc(userId);
    return new PureeRef(ref);
  }

  public userDevice(userId: string): UserDeviceRef {
    // validation
    if (!userId) {
      throw new StandardError(
        "invalid-argument",
        "UserDeviceRef requires userId"
      );
    }

    // create ref
    const ref = this.db.collection("userDevices").doc(userId);
    return new PureeRef(ref);
  }

  /*
    stamp
  */

  public newStamp(): StampRef {
    // create ref
    const ref = this.db.collection("stamps").doc();
    return new PureeRef(ref);
  }

  /*
    stamp stock
  */

  public stampStock(userId: string): StampStockRef {
    // create ref
    const ref = this.db.collection("stampStocks").doc(userId);
    return new PureeRef(ref);
  }

  /*
    stamp set
  */

  public stampSets(props: { venderId?: string }): StampSetQuery {
    // create query
    let ref: Query = this.db.collection("stampSets").limit(20);

    // vender filter (if present)
    if (props.venderId) {
      ref = ref.where("vendor", "==", props.venderId);
    }

    return new PureeQuery(ref);
  }

  /*
    stamp set
  */

  public newStampSet(): StampSetRef {
    // create ref
    const ref = this.db.collection("stampSets").doc();
    return new PureeRef(ref);
  }

  public stampSet(stampSetId: string): StampSetRef {
    // create ref
    const ref = this.db.collection("stampSets").doc(stampSetId);
    return new PureeRef(ref);
  }

  /*
    stamp order
  */

  public newStampOrder(): StampOrderRef {
    // create ref
    const ref = this.db.collection("stampOrders").doc();
    return new PureeRef(ref);
  }
}

export default RefGenerator;

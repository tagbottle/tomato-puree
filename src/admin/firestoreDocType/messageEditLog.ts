import { Reference, Time } from "./_util";

export interface IMessageEditLog {
  message?: Reference;
  bottle?: Reference;
  timestamp?: Time;
  user?: Reference;
  text?: string;
}

export interface IMessageEditLogDocument extends IMessageEditLog {
  bottle: Reference;
  message: Reference;
  timestamp: Time;
  user: Reference;
  text: string;
}

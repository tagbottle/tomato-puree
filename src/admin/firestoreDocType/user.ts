import { CommonType } from "./_util";

export type IUser = CommonType.IUser;
export type IUserDocument = CommonType.IUserDocument;

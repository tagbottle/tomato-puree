import { CommonType } from "./_util";

export type IUsername = CommonType.IUsername;
export type IUsernameDocument = CommonType.IUsernameDocument;

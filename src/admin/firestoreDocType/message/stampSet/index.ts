import { CommonType, Time } from "../../_util";

export interface IMessageStampSet extends CommonType.IMessageStampSet {
  stampItems?: { [key: string]: IMessageStampSetItem };
}
export interface IMessageStampSetDocument
  extends CommonType.IMessageStampSetDocument {
  stampItems: { [key: string]: IMessageStampSetItem };
}

/*
  item of stamp set
 */

export interface IMessageStampSetItem extends CommonType.IMessageStampSetItem {
  lastUsed?: Time;
}

export interface IMessageStampSetItemDocument
  extends CommonType.IMessageStampSetItemDocument {
  lastUsed: Time;
}

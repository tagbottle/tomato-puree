import { CommonType } from "../../../_util";
export interface IMessageStampRequest extends CommonType.IMessageStampRequest {
  stampItemId?: string;
}

export interface IMessageStampRequestDocument
  extends CommonType.IMessageStampRequestDocument {
  stampItemId: string;
}

import { CommonType, DocumentReference, Reference, Time } from "../_util";

export interface IMessage extends CommonType.IMessage {
  bottle?: Reference;
  timestamp?: Time;
  user?: Reference;
  stamps?: IStampSummary[];
}

export interface IMessageDocument extends CommonType.IMessageDocument {
  bottle: DocumentReference;
  timestamp: Time;
  user: DocumentReference;
  stamps: IStampSummaryDocument[];
}

/*
  stamp summary
 */
export interface IStampSummary extends CommonType.IStampSummary {
  stampSet?: Reference;
}

export interface IStampSummaryDocument
  extends CommonType.IStampSummaryDocument {
  stampSet: DocumentReference;
}

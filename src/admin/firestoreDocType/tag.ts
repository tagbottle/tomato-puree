import { CommonType, DocumentReference, Reference } from "./_util";

export interface ITag extends CommonType.ITag {
  currentBottle?: Reference;
  bottles?: { [key: number]: Reference };
}

export interface ITagDocument extends CommonType.ITagDocument {
  currentBottle: DocumentReference;
  bottles: { [key: number]: DocumentReference };
}

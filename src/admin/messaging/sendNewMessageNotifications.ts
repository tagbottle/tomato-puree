import admin from "firebase-admin";
import { either } from "fp-ts";
import validate from "validate.js";

import * as resultType from "../../type/result";
import { Db } from "../client";

import { INewMessageNotification } from "../../common/appTypes";
import tagSubscribeToken from "../../util/convert/messaging/tagSubscribeToken";
import encodeThemesToTagString from "../../util/convert/tag/encodeThemesToTagString";

export interface IProps {
  bottleId: string;
  messageId: string;

  index: number;
  themes: string[];
  text: string;

  userId: string;
  username: string;
  displayName: string;
  photoUrl: string;
}

export interface ISuccessPayload {
  notificationMessageId: string;
}

type Result = resultType.StandardResult<
  "ok" | "invalid-argument",
  ISuccessPayload
>;

export const sendNewMessageNotifications = async (
  db: Db,
  messaging: admin.messaging.Messaging,
  props: IProps
): Promise<Result> => {
  const {
    bottleId,
    displayName,
    index,
    messageId,
    photoUrl,
    text,
    themes,
    userId,
    username
  } = props;

  // notification
  const topic = tagSubscribeToken(themes);
  const encodedTag = encodeThemesToTagString(themes)!;
  const notification: INewMessageNotification = {
    data: {
      bottleId,
      displayName,
      encodedTag,
      index: index.toString(),
      messageId,
      photoUrl,
      text,
      userId,
      username
    },
    topic
  };

  // validation
  if (!validate.isString(notification.data.bottleId)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.bottleId is not string"
    );
  }
  if (!validate.isString(notification.data.displayName)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.displayName is not string"
    );
  }
  if (!validate.isString(notification.data.encodedTag)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.encodedTag is not string"
    );
  }
  if (!validate.isString(notification.data.index)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.index is not string"
    );
  }
  if (!validate.isString(notification.data.messageId)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.messageId is not string"
    );
  }
  if (!validate.isString(notification.data.photoUrl)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.photoUrl is not string"
    );
  }
  if (!validate.isString(notification.data.text)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.text is not string"
    );
  }
  if (!validate.isString(notification.data.userId)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.userId is not string"
    );
  }
  if (!validate.isString(notification.data.username)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.username is not string"
    );
  }
  if (!validate.isString(notification.topic)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.topic is not string"
    );
  }

  const notificationMessageId = await messaging.send(notification);
  return resultType.right({ notificationMessageId });
};

export default sendNewMessageNotifications;

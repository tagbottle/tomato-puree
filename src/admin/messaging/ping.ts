import admin from "firebase-admin";
import { either } from "fp-ts";
import validate from "validate.js";

import * as resultType from "../../type/result";
import { Db } from "../client";

import { IPingNotification } from "../../common/messaging";
import encodeThemesToTagString from "../../util/convert/tag/encodeThemesToTagString";
import { RefGenerator, UserRef } from "../ref";

export interface IProps {
  themes: string[];
  pingUserRef: UserRef;
  targetUserRef: UserRef;
}

export interface ISuccessPayload {
  status: string;
}

type ErrorCodes = "invalid-argument" | "unavailable" | "unknown";
type Error = resultType.StandardError<ErrorCodes>;

type Result = resultType.StandardResult<ErrorCodes, ISuccessPayload>;

function validateProps(props: IProps): either.Either<Error, IProps> {
  if (!props.themes) {
    return resultType.leftError("invalid-argument", "props.themes is blank");
  }

  if (!props.pingUserRef) {
    return resultType.leftError(
      "invalid-argument",
      "props.pingUserRef is blank"
    );
  }

  if (!props.targetUserRef) {
    return resultType.leftError(
      "invalid-argument",
      "props.targetUserRef is blank"
    );
  }

  return either.right(props);
}

export const ping = async (
  db: Db,
  messaging: admin.messaging.Messaging,
  props: IProps
): Promise<Result> => {
  // validate
  const validateResult = validateProps(props);
  if (validateResult.isLeft()) {
    return either.left(validateResult.value);
  }
  const { themes, pingUserRef, targetUserRef } = props;

  // get related docs
  // 1. ping sending user
  const pingUserDoc = await pingUserRef.data();
  if (!pingUserDoc.snapshot.exists || !pingUserDoc.data) {
    return resultType.leftError("unknown", "pingUserDoc is blank");
  }

  // 2. ping target user's devices
  const refGenerator = new RefGenerator(db);
  const targetUserDeviceRef = refGenerator.userDevice(targetUserRef.ref.id);
  const targetUserDeviceDoc = await targetUserDeviceRef.data();
  if (!targetUserDeviceDoc.snapshot.exists || !targetUserDeviceDoc.data) {
    return resultType.leftError("unknown", "targetUserDeviceDoc is blank");
  }
  if (
    !targetUserDeviceDoc.data.devices ||
    !targetUserDeviceDoc.data.devices.length
  ) {
    return resultType.leftError(
      "unavailable",
      "targetUserDeviceDoc.data.devices is blank"
    );
  }

  // create notification data
  const encodedTag = encodeThemesToTagString(themes)!;
  const notification: IPingNotification = {
    data: {
      encodedTag,
      iconUrl: pingUserDoc.data.photoUrl,
      pingDisplayName: pingUserDoc.data.displayName,
      pingUsername: pingUserDoc.data.username || ""
    }
  };

  // validate notification
  if (!validate.isString(notification.data.encodedTag)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.encodedTag is not string"
    );
  }

  if (!validate.isString(notification.data.iconUrl)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.iconUrl is not string"
    );
  }

  if (!validate.isString(notification.data.pingDisplayName)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.pingDisplayName is not string"
    );
  }

  if (!validate.isString(notification.data.pingUsername)) {
    return resultType.leftError(
      "invalid-argument",
      "notification.data.pingUsername is not string"
    );
  }

  // send a message
  const deviceTokens = targetUserDeviceDoc.data.devices
    .filter(d => d.active)
    .map(d => d.token);
  const notificationMessageId = await messaging.sendToDevice(
    deviceTokens,
    notification
  );
  return resultType.right({ status: "ok" });
};

export default ping;

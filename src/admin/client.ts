import firestore from "@google-cloud/firestore";

export { firestore as Firestore };
export type Db = firestore.Firestore;
export type DocumentReference = firestore.DocumentReference;
export type Query = firestore.Query;
export type DocumentSnapshot = firestore.DocumentSnapshot;
export type QuerySnapshot = firestore.QuerySnapshot;
export type Precondition = firestore.Precondition;
export type Transaction = firestore.Transaction;
export type FieldValue = firestore.FieldValue;
export type SetOptions = firestore.SetOptions;
export type UpdateData = firestore.UpdateData;
export type WriteResult = firestore.WriteResult;

import { either } from "fp-ts";
import validate from "validate.js";

import * as resultType from "../../type/result";
import isInvalidStampSet from "../../util/validate/stamp/isInvalidStampSet";
import { Db } from "../client";
import { RefGenerator } from "../ref";

export interface IProps {
  vendor: string;
  title: string;
  items: Array<{
    id: string;
    index: number;
    name: string;
    url: string;
  }>;
}

export interface ISuccessPayload {
  stampSetId: string;
  stampSetPath: string;
}

type Result = resultType.StandardResult<"invalid-argument", ISuccessPayload>;

export const registerStampSet = async (
  db: Db,
  props: IProps
): Promise<Result> => {
  // validate
  if (isInvalidStampSet(props)) {
    return resultType.leftError("invalid-argument");
  }

  // util setup
  const refGenerator = new RefGenerator(db);

  // create new doc
  const stampSetRef = refGenerator.newStampSet();
  await stampSetRef.create(props);

  return resultType.right({
    stampSetId: stampSetRef.ref.id,
    stampSetPath: stampSetRef.ref.path
  });
};

export default registerStampSet;

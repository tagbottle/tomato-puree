import { firestore } from "firebase-admin";
import { either } from "fp-ts";
import validate from "validate.js";

import * as resultType from "../../type/result";
import { Db, FieldValue, Firestore, Transaction } from "../client";
import {
  IMessageDocument,
  IMessageStampRequestDocument,
  IStampSummaryDocument
} from "../firestoreDocType";
import {
  MessageRef,
  MessageStampRequestRefList,
  PureeRefList,
  RefGenerator,
  StampSetRef
} from "../ref";

export interface IProps {
  messageRef: MessageRef;
  stampSetRef: StampSetRef;
}

type ErrorCodes = "not-found" | "unknown" | "invalid-argument";
interface ISuccessPayload {
  amount: number;
}
type Result = resultType.StandardResult<ErrorCodes, ISuccessPayload>;

function validateProps(props: IProps): string | null {
  if (!props.stampSetRef) {
    return "props.stampSetRef is null";
  }
  if (!props.messageRef) {
    return "props.messageRef is null";
  }
  return null;
}

/*
  read data in transaction
 */

interface IRequestRelatedRefs {
  messageRef: MessageRef;
  messageStampRequestRefList: MessageStampRequestRefList;
  stampSetRef: StampSetRef;
}
type ReadDataResult = either.Either<
  resultType.StandardError<ErrorCodes>,
  IReadDataInTransaction
>;
interface IReadDataInTransaction {
  messageData: IMessageDocument;
  messageStampRequestDataList: IMessageStampRequestDocument[];
}

async function getData(
  transaction: Transaction,
  refs: IRequestRelatedRefs
): Promise<ReadDataResult> {
  const dataList = await Promise.all([
    refs.messageRef.dataInTransaction(transaction),
    refs.messageStampRequestRefList.dataInTransaction(transaction)
  ]);

  // 1. message doc
  const messageDataResult = dataList[0];
  if (!messageDataResult.data) {
    return resultType.leftError("not-found", "messageDataResult.data is blank");
  }

  // 2. stamp set doc
  const messageStampRequestDataListResult = dataList[1];
  const messageStampRequestDataList: IMessageStampRequestDocument[] = [];

  for (const r of messageStampRequestDataListResult) {
    if (!r.snapshot.exists || !r.data) {
      continue;
    }
    messageStampRequestDataList.push(r.data);
  }

  return either.right({
    messageData: messageDataResult.data,
    messageStampRequestDataList
  });
}

/*
  validation in transaction
*/

type ValidationResult = either.Either<
  resultType.StandardError<ErrorCodes>,
  IReadDataInTransaction
>;

function validateDocs(
  props: IProps,
  docs: IReadDataInTransaction
): ValidationResult {
  // 1. message doc
  // no check

  // 2. stamp set doc
  if (docs.messageStampRequestDataList.length === 0) {
    const error = new resultType.StandardError(
      "not-found",
      "docs.messageStampRequestDataList is empty"
    );
    return either.left(error);
  }

  // all ok!
  return either.right(docs);
}

/*
  update in transaction
*/

type UpdateResult = either.Either<
  resultType.StandardError<ErrorCodes>,
  ISuccessPayload
>;

function createNewStampsList(
  stampSetRef: StampSetRef,
  stampList: IStampSummaryDocument[],
  requests: IMessageStampRequestDocument[]
): IStampSummaryDocument[] {
  const result: IStampSummaryDocument[] = [];
  result.push(...stampList);

  for (const request of requests) {
    // find same stamp
    const sameStampIndex = result.findIndex(
      s =>
        s.stampSet.id === stampSetRef.ref.id &&
        s.stampItemId === request.stampItemId
    );

    // add new item if none found
    if (sameStampIndex === -1) {
      result.push({
        amount: 1,
        stampItemId: request.stampItemId,
        stampSet: stampSetRef.ref
      });
      continue;
    }

    // new stamp info (+1 amount)
    const updatedStampInfo = {
      amount: result[sameStampIndex].amount + 1,
      stampItemId: request.stampItemId,
      stampSet: stampSetRef.ref
    };

    // remove current info
    result.splice(sameStampIndex, 1);

    // add new info
    result.unshift(updatedStampInfo);
  }
  return result;
}

async function updateDocs(
  validationResult: ValidationResult,
  transaction: Transaction,
  props: IProps,
  refs: IRequestRelatedRefs
): Promise<UpdateResult> {
  // if validation has failed, break updating
  if (validationResult.isLeft()) {
    return either.left(validationResult.value);
  }

  // use docs, which are loaded in transaction
  const docs = validationResult.value;

  // update message.stamp
  const currentStampsList = docs.messageData.stamps;
  const newStampsList = createNewStampsList(
    refs.stampSetRef,
    currentStampsList,
    docs.messageStampRequestDataList
  );
  const messageUpdate = refs.messageRef.updateInTransaction(transaction, {
    stamps: newStampsList
  });

  // delete all requests
  const messageStampRequestRefListDelete = refs.messageStampRequestRefList.deleteAllInTransaction(
    transaction
  );

  await Promise.all([messageUpdate, messageStampRequestRefListDelete]);

  // all ok!
  return either.right({ amount: docs.messageStampRequestDataList.length });
}

/*
  main
 */

export const callConsumptionTransaction = async (
  db: Db,
  props: IProps
): Promise<Result> => {
  const { messageRef, stampSetRef } = props;

  // util setup
  const refGenerator = new RefGenerator(db);

  // validate props
  const errorMessage = validateProps(props);
  if (errorMessage) {
    return resultType.leftError("invalid-argument", errorMessage);
  }

  // create required refs
  // TODO: use message.stampSet doc to scale

  // get all requests
  const messageStampRequests = await refGenerator
    .messageStampRequests(messageRef.ref.id, stampSetRef.ref.id)
    .data();
  if (messageStampRequests.list.length === 0) {
    return resultType.right({ amount: 0 });
  }
  const messageStampRequestRefs = messageStampRequests.list.map(d => d.ref);
  const messageStampRequestRefList = new PureeRefList(messageStampRequestRefs);
  const refs: IRequestRelatedRefs = {
    messageRef,
    messageStampRequestRefList,
    stampSetRef
  };

  return await db.runTransaction(
    async (transaction): Promise<Result> => {
      // get data
      const getDataResult: ReadDataResult = await getData(transaction, refs);

      // validate
      const validationResult: ValidationResult = getDataResult.fold(
        error => either.left(error),
        docs => validateDocs(props, docs)
      );

      // update
      return await updateDocs(validationResult, transaction, props, refs);
    }
  );
};

export default callConsumptionTransaction;

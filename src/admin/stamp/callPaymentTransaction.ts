import { either } from "fp-ts";
import validate from "validate.js";

import * as resultType from "../../type/result";
import { Db, FieldValue } from "../client";
import {
  IStampOrderDocument,
  IStampStockDocument,
  IStampStockReceiptDocument,
  IStampStockStampSetDocument
} from "../firestoreDocType";
import { RefGenerator, StampOrderRef, StampSetRef } from "../ref";

export interface IProps {
  stampOrderRef: StampOrderRef;
  timestamp: FieldValue;
}

export interface ISuccessPayload {
  stampOrderRef: StampOrderRef;
}

type Result = resultType.StandardResult<
  "invalid-argument" | "unknown" | "not-found",
  ISuccessPayload
>;
type FlagOverwriteTransactionResult = resultType.StandardResult<
  "invalid-argument" | "unknown" | "not-found",
  { stampOrderDoc: IStampOrderDocument }
>;
type PaymentUpdateTransactionResult = resultType.StandardResult<
  "invalid-argument" | "unknown" | "not-found",
  null
>;

/*
    update
 */

function createNewStampStockDoc(
  current: IStampStockDocument,
  stampSetRef: StampSetRef,
  amount: number,
  maxLength = 20
): IStampStockDocument {
  const result: IStampStockDocument = {
    favorites: [...current.favorites],
    latestBought: [],
    latestUsed: [...current.latestUsed]
  };

  // add new `latest bought` info
  result.latestBought.push({
    amount,
    stampSet: stampSetRef.ref
  });
  const otherLatestBought = current.latestBought
    .filter(u => u.stampSet.path !== stampSetRef.ref.path)
    .slice(0, maxLength - 1);
  result.latestBought.push(...otherLatestBought);

  // update amount in other list
  for (const index in result.favorites) {
    if (result.favorites[index].stampSet.path === stampSetRef.ref.path) {
      result.favorites[index].amount = amount;
    }
  }
  for (const index in result.latestUsed) {
    if (result.latestUsed[index].stampSet.path === stampSetRef.ref.path) {
      result.latestUsed[index].amount = amount;
    }
  }

  return result;
}

export const callPaymentTransaction = async (
  db: Db,
  props: IProps
): Promise<Result> => {
  // util setup
  const { timestamp, stampOrderRef } = props;
  const refGenerator = new RefGenerator(db);

  // set `working` flag
  const flagOverwriteTransactionResult = await db.runTransaction(
    async (transaction): Promise<FlagOverwriteTransactionResult> => {
      // get order doc
      const stampOrderDataResult = await stampOrderRef.dataInTransaction(
        transaction
      );

      // validate
      if (!stampOrderDataResult.data) {
        return resultType.leftError(
          "not-found",
          "stampOrderDataResult.data not found"
        );
      }
      if (stampOrderDataResult.data.status !== "pending") {
        return resultType.leftError(
          "unknown",
          `stampOrderDataResult.data.status is ${
            stampOrderDataResult.data.status
          }`
        );
      }

      // overwrite order's flag
      await stampOrderRef.updateInTransaction(transaction, {
        status: "working"
      });

      return resultType.right({
        stampOrderDoc: stampOrderDataResult.data
      });
    }
  );
  if (flagOverwriteTransactionResult.isLeft()) {
    return either.left(flagOverwriteTransactionResult._L);
  }

  // get contents of order
  const { stampOrderDoc } = flagOverwriteTransactionResult.value;
  const userRef = refGenerator.user(stampOrderDoc.user.id);
  const stampSetRef = refGenerator.stampSet(stampOrderDoc.stampSet.id);
  if (userRef == null) {
    return resultType.leftError("unknown", "userRef is null");
  }
  if (stampSetRef == null) {
    return resultType.leftError("unknown", "stampSetRef is null");
  }

  // TODO: call stripe api
  const pay = async (doc: any) => true;
  const paymentSuccess = await pay(stampOrderDoc);
  if (!paymentSuccess) {
    await stampOrderRef.update({ status: "failed" });
    return resultType.leftError("unknown", "payment failed");
  }

  // create payment transaction receipt
  const stampReceiptRef = refGenerator.newStampReceipt();
  try {
    await stampReceiptRef.create({
      createdAt: timestamp,
      order: stampOrderRef.ref,
      stampSet: stampSetRef.ref,
      status: "ok",
      transactionId: "test-transaction-id",
      user: userRef.ref
    });
  } catch (e) {
    await stampOrderRef.update({ status: "failed" });
    return resultType.leftError(
      "unknown",
      `stampReceiptRef.create failed: ${e.message}`
    );
  }

  // update related documents
  const paymentUpdateTransaction = db.runTransaction(
    async (transaction): Promise<PaymentUpdateTransactionResult> => {
      // get order doc (again)
      const stampOrderDataResult = await stampOrderRef.dataInTransaction(
        transaction
      );
      if (!stampOrderDataResult) {
        return resultType.leftError(
          "unknown",
          "stampOrderDataResult is removed"
        );
      }

      // get other related docs
      // (1) stamp stock of user
      const stampStockRef = refGenerator.stampStock(userRef.ref.id);
      const stampStockDataResult = await stampStockRef.dataInTransaction(
        transaction
      );

      // (2) stamp set info in stamp stock
      const stampStockStampSetRef = refGenerator.stampStockStampSet(
        userRef.ref.id,
        stampSetRef.ref.id
      );
      const stampStockStampSetDataResult = await stampStockStampSetRef.dataInTransaction(
        transaction
      );

      // (3) receipt info in stamp stock
      const stampStockReceiptRef = refGenerator.stampStockReceipt(
        userRef.ref.id,
        stampSetRef.ref.id,
        stampReceiptRef.ref.id
      );
      const stampStockReceiptDataResult = await stampStockReceiptRef.dataInTransaction(
        transaction
      );

      // create & update related docs
      const promises: Array<Promise<any>> = [];

      // (1) stamp set info in stamp stock
      let totalAmount = null;
      if (stampStockStampSetDataResult.data) {
        const stampStockStampSetDoc = stampStockStampSetDataResult.data;
        totalAmount = stampStockStampSetDoc.amount + stampOrderDoc.amount;
        const stampStockStampSetUpdate = stampStockStampSetRef.updateInTransaction(
          transaction,
          {
            amount: totalAmount
          }
        );
        promises.push(stampStockStampSetUpdate);
      } else {
        totalAmount = stampOrderDoc.amount;
        const stampStockStampSetDoc: IStampStockStampSetDocument = {
          amount: stampOrderDoc.amount,
          favorite: false,
          lastBoughtAt: timestamp,
          lastUsedAt: null
        };
        const stampStockStampSetRefCreate = stampStockStampSetRef.createInTransaction(
          transaction,
          stampStockStampSetDoc
        );
        promises.push(stampStockStampSetRefCreate);
      }

      // (2) stamp stock of user
      if (stampStockDataResult.data) {
        const stampStockDoc = stampStockDataResult.data;

        // create new stamp-stock
        const newStampStockDoc: IStampStockDocument = createNewStampStockDoc(
          stampStockDoc,
          stampSetRef,
          totalAmount
        );

        const stampStockRefUpdate = stampStockRef.updateInTransaction(
          transaction,
          newStampStockDoc
        );
        promises.push(stampStockRefUpdate);
      } else {
        const stampStockDoc: IStampStockDocument = {
          favorites: [],
          latestBought: [
            {
              amount: totalAmount,
              stampSet: stampSetRef.ref
            }
          ],
          latestUsed: []
        };
        const stampStockRefCreate = stampStockRef.createInTransaction(
          transaction,
          stampStockDoc
        );
        promises.push(stampStockRefCreate);
      }

      // (3) receipt info in stamp stock
      const stampStockReceiptDoc: IStampStockReceiptDocument = {
        amount: stampOrderDoc.amount,
        boughtAt: timestamp,
        logs: []
      };
      const stampStockReceiptCreate = stampStockReceiptRef.createInTransaction(
        transaction,
        stampStockReceiptDoc
      );
      promises.push(stampStockReceiptCreate);

      // (4) overwrite order doc
      const stampOrderUpdate = stampOrderRef.updateInTransaction(transaction, {
        status: "ok"
      });
      promises.push(stampOrderUpdate);

      await Promise.all(promises);
      return either.right(null);
    }
  );

  try {
    // run transaction
    await paymentUpdateTransaction;
  } catch (e) {
    // if transaction failed, cancel payment
    const cancel = async () => true;
    await cancel();

    // update stamp-order with `failed` status
    await stampOrderRef.update({ status: "failed" });

    // return left error
    return resultType.leftError("unknown", `promise(s) failed: ${e.message}`);
  }

  return resultType.right({ stampOrderRef });
};

export default callPaymentTransaction;

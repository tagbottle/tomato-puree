import { firestore } from "firebase-admin";
import { either } from "fp-ts";
import validate from "validate.js";

import * as resultType from "../../type/result";
import { Db, FieldValue, Transaction } from "../client";
import {
  IStampDocument,
  IStampSetDocument,
  IStampStockDocument,
  IStampStockReceiptDocument,
  IStampStockStampSetDocument
} from "../firestoreDocType";
import {
  RefGenerator,
  StampRef,
  StampSetRef,
  StampStockReceiptRef,
  StampStockRef,
  StampStockStampSetRef
} from "../ref";

export interface IProps {
  stampRef: StampRef;
  stampDoc: IStampDocument;
  timestamp: FieldValue;
}

export interface ISuccessPayload {
  stampRef: StampRef;
}

type ErrorCodes = "invalid-argument" | "unknown" | "not-found" | "unavailable";
type Result = resultType.StandardResult<ErrorCodes, ISuccessPayload>;

function validateProps(props: IProps): string | null {
  return null;
}

/*
  read data in transaction
 */

interface IReadDataInTransaction {
  stampData: IStampDocument;
  stampSetData: IStampSetDocument;
  stampStockData: IStampStockDocument;
  stampStockReceiptData: IStampStockReceiptDocument;
  stampStockStampSetData: IStampStockStampSetDocument;
}
interface IStampRelatedRefs {
  stampRef: StampRef;
  stampSetRef: StampSetRef;
  stampStockRef: StampStockRef;
  stampStockStampSetRef: StampStockStampSetRef;
  stampStockReceiptRef: StampStockReceiptRef;
}
type ReadDataResult = either.Either<
  resultType.StandardError<ErrorCodes>,
  IReadDataInTransaction
>;

async function getData(
  transaction: Transaction,
  refs: IStampRelatedRefs
): Promise<ReadDataResult> {
  const dataList = await Promise.all([
    refs.stampRef.dataInTransaction(transaction),
    refs.stampSetRef.dataInTransaction(transaction),
    refs.stampStockRef.dataInTransaction(transaction),
    refs.stampStockStampSetRef.dataInTransaction(transaction),
    refs.stampStockReceiptRef.dataInTransaction(transaction)
  ]);

  // 1. stamp doc
  const stampDataResult = dataList[0];
  if (!stampDataResult.data) {
    return resultType.leftError("not-found", "stampDataResult.data is blank");
  }

  // 2. stamp set doc
  const stampSetDataResult = dataList[1];
  if (!stampSetDataResult.data) {
    return resultType.leftError(
      "not-found",
      "stampSetDataResult.data is blank"
    );
  }

  // 3. stampStock doc
  const stampStockDataResult = dataList[2];
  if (!stampStockDataResult.data) {
    return resultType.leftError(
      "not-found",
      "stampStockDataResult.data is blank"
    );
  }

  // 4. stampStock-stampSet doc
  const stampStockStampSetDataResult = dataList[3];
  if (!stampStockStampSetDataResult.data) {
    return resultType.leftError(
      "not-found",
      "stampStockStampSetDataResult.data is blank"
    );
  }

  // 5. receipt log doc
  const stampStockReceiptDataResult = dataList[4];
  if (!stampStockReceiptDataResult.data) {
    return resultType.leftError(
      "not-found",
      "stampStockReceiptDataResult.data is blank"
    );
  }

  return resultType.right({
    stampData: stampDataResult.data,
    stampSetData: stampSetDataResult.data,
    stampStockData: stampStockDataResult.data,
    stampStockReceiptData: stampStockReceiptDataResult.data,
    stampStockStampSetData: stampStockStampSetDataResult.data
  });
}

/*
  validation in transaction
*/

type ValidationResult = either.Either<
  resultType.StandardError<ErrorCodes>,
  IReadDataInTransaction
>;

function validateDocs(
  props: IProps,
  docs: IReadDataInTransaction
): ValidationResult {
  // 1. stamp doc
  if (docs.stampData.status !== "pending") {
    const error = new resultType.StandardError(
      "unknown",
      "docs.stampData.status is not pending"
    );
    return either.left(error);
  }
  // 2. stamp set doc
  const stampItem = docs.stampSetData.items.find(
    item => item.id === props.stampDoc.stampItemId
  );
  if (!stampItem) {
    const error = new resultType.StandardError(
      "not-found",
      "docs.stampSetData.items does not contain props.stampDoc.stampItemId"
    );
    return either.left(error);
  }

  // 3. stampStock doc
  // no check

  // 4. stampStock-stampSet doc
  // no check

  // 5. receipt log doc
  if (docs.stampStockReceiptData.amount === 0) {
    const error = new resultType.StandardError(
      "unknown",
      "docs.stampStockReceiptData.amount is 0"
    );
    return either.left(error);
  }

  // all ok!
  return either.right(docs);
}

/*
  update in transaction
*/

type UpdateResult = either.Either<
  resultType.StandardError<ErrorCodes>,
  ISuccessPayload
>;

function createNewStampStockDoc(
  current: IStampStockDocument,
  stampSetRef: StampSetRef,
  amount: number,
  maxLength = 20
): IStampStockDocument {
  const result: IStampStockDocument = {
    favorites: [...current.favorites],
    latestBought: [...current.latestBought],
    latestUsed: []
  };

  // add new `latest used` info
  result.latestUsed.push({
    amount,
    stampSet: stampSetRef.ref
  });
  const otherLatestUsed = current.latestUsed
    .filter(u => u.stampSet.path !== stampSetRef.ref.path)
    .slice(0, maxLength - 1);
  result.latestUsed.push(...otherLatestUsed);

  // update amount in other list
  for (const index in result.favorites) {
    if (result.favorites[index].stampSet.path === stampSetRef.ref.path) {
      result.favorites[index].amount = amount;
    }
  }
  for (const index in result.latestBought) {
    if (result.latestBought[index].stampSet.path === stampSetRef.ref.path) {
      result.latestBought[index].amount = amount;
    }
  }

  return result;
}

async function updateDocs(
  validationResult: ValidationResult,
  transaction: Transaction,
  props: IProps,
  refs: IStampRelatedRefs
): Promise<UpdateResult> {
  // if validation has failed, break updating
  if (validationResult.isLeft()) {
    return either.left(validationResult.value);
  }

  // use docs, which are loaded in transaction
  const docs = validationResult.value;

  // update receipt in stamp stock
  const amountInReceipt = docs.stampStockReceiptData.amount - 1;
  const stampLogs = docs.stampStockReceiptData.logs.concat({
    message: docs.stampData.message,
    stampItemId: docs.stampData.stampItemId
  });
  const stampStockReceiptUpdate = refs.stampStockReceiptRef.updateInTransaction(
    transaction,
    {
      amount: amountInReceipt,
      logs: stampLogs
    }
  );

  // update stamp-stock's stamp-set info
  const amountInStampSet = docs.stampStockStampSetData.amount - 1;
  const stampStockStampSetUpdate = refs.stampStockStampSetRef.updateInTransaction(
    transaction,
    {
      amount: amountInStampSet,
      lastUsedAt: props.timestamp
    }
  );

  // update stamp-stock
  const newStampStockDoc = createNewStampStockDoc(
    docs.stampStockData,
    refs.stampSetRef,
    amountInStampSet
  );
  const stampStockUpdate = refs.stampStockRef.updateInTransaction(transaction, {
    favorites: newStampStockDoc.favorites,
    latestBought: newStampStockDoc.latestBought,
    latestUsed: newStampStockDoc.latestUsed
  });

  // update stamp doc and set `ok` status
  const stampUpdate = refs.stampRef.updateInTransaction(transaction, {
    status: "ok"
  });

  await Promise.all([
    stampStockReceiptUpdate,
    stampStockStampSetUpdate,
    stampStockUpdate,
    stampUpdate
  ]);

  // all ok!
  return either.right({ stampRef: refs.stampRef });
}

/*
  main
 */

export const callConsumptionTransaction = async (
  db: Db,
  props: IProps
): Promise<Result> => {
  const { stampRef, stampDoc } = props;

  // util setup
  const refGenerator = new RefGenerator(db);

  // validate props
  const errorMessage = validateProps(props);
  if (errorMessage) {
    await stampRef.update({ status: "failed" });
    return resultType.leftError("invalid-argument", errorMessage);
  }

  // create required refs
  const stampSetRef = refGenerator.stampSet(stampDoc.stampSet.id);
  const stampStockRef = refGenerator.stampStock(stampDoc.user.id);
  const stampStockStampSetRef = refGenerator.stampStockStampSet(
    stampDoc.user.id,
    stampDoc.stampSet.id
  );

  // get receipt log to consume
  const stampStockReceipts = await refGenerator
    .stampStockReceipts(stampDoc.user.id, stampDoc.stampSet.id)
    .data();
  if (stampStockReceipts.list.length === 0) {
    await stampRef.update({ status: "failed" });
    return resultType.leftError("unavailable", "no available receipt");
  }
  const stampStockReceiptRef = stampStockReceipts.list[0].ref;

  const refs: IStampRelatedRefs = {
    stampRef,
    stampSetRef,
    stampStockReceiptRef,
    stampStockRef,
    stampStockStampSetRef
  };

  const result = await db.runTransaction(
    async (transaction): Promise<Result> => {
      // get data
      const getDataResult: ReadDataResult = await getData(transaction, refs);

      // validate
      const validationResult: ValidationResult = getDataResult.fold(
        error => either.left(error),
        docs => validateDocs(props, docs)
      );

      // update
      const updateResult: UpdateResult = await updateDocs(
        validationResult,
        transaction,
        props,
        refs
      );

      // if some error happens, set stamp status as `failed`
      if (updateResult.isLeft()) {
        await stampRef.updateInTransaction(transaction, { status: "failed" });
      }

      // all done!
      return updateResult;
    }
  );

  // return error  if transaction failed
  if (result.isLeft()) {
    return result;
  }

  // create request to display stamp
  const messageStampRequestRef = refGenerator.newMessageStampRequest(
    stampDoc.message.id,
    stampDoc.stampSet.id
  );
  await messageStampRequestRef.create({
    stampItemId: stampDoc.stampItemId
  });

  return result;
};

export default callConsumptionTransaction;

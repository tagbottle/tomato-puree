import { either } from "fp-ts";
import validate from "validate.js";
import { UserRef } from "../../admin/ref/pureeRef";
import * as resultType from "../../type/result";
import { Firestore } from "../client";
import * as DocTypes from "../firestoreDocType";
import { PureeRef, RefGenerator } from "../ref";

export interface IProps {
  displayName: string;
  photoUrl: string;
  uid: string;
  token: string;
}

export interface ISuccessPayload {
  userRef: UserRef;
  data: IProps;
}

type Result = resultType.StandardResult<
  "not-found" | "invalid-argument" | "forbidden",
  ISuccessPayload
>;

export const createUser = async (
  db: Firestore.Firestore,
  data: IProps
): Promise<Result> => {
  // check argument
  const constraints = {
    displayName: {
      length: {
        minimum: 1
      },
      presence: true
    },
    photoUrl: {
      presence: true,
      url: {
        schemes: ["https"]
      }
    }
  };
  const invalidArguments = validate(data, constraints);
  if (invalidArguments) {
    return resultType.leftError("invalid-argument");
  }

  const { displayName, photoUrl, uid } = data;

  // create user doc
  const refManager = new RefGenerator(db);
  const userRef = refManager.user(uid)!;
  await userRef.set({ displayName, photoUrl }, { merge: true });

  return resultType.right({ userRef, data });
};

export default createUser;

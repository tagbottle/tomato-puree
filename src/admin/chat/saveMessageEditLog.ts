import { either } from "fp-ts";

import { MessageEditLogRef, MessageRef } from "../../admin/ref";
import * as resultType from "../../type/result";
import { FieldValue, Firestore } from "../client";
import * as DocTypes from "../firestoreDocType";
import { RefGenerator } from "../ref";

export interface IProps {
  messageRef: MessageRef;
  messageDoc: DocTypes.IMessageDocument;
  timestamp: FieldValue;
}

export interface ISuccessPayload {
  messageEditLogRef: MessageEditLogRef;
  messageEditLogDoc: DocTypes.IMessageEditLogDocument;
}

type Errors = "invalid-argument" | "unknown";
type Result = resultType.StandardResult<Errors, ISuccessPayload>;

function validateProps(
  props: IProps
): either.Either<resultType.StandardError<Errors>, IProps> {
  if (!props.messageRef) {
    return resultType.leftError(
      "invalid-argument",
      "props.messageRef is blank"
    );
  }
  if (!props.messageDoc) {
    return resultType.leftError(
      "invalid-argument",
      "props.messageDoc is blank"
    );
  }
  if (!props.timestamp) {
    return resultType.leftError("invalid-argument", "props.timestamp is blank");
  }

  return either.right(props);
}

export const createTag = async (
  db: Firestore.Firestore,
  props: IProps
): Promise<Result> => {
  // validate
  const validateResult = validateProps(props);
  if (validateResult.isLeft()) {
    return either.left(validateResult.value);
  }

  // create ref
  const { messageRef, messageDoc, timestamp } = validateResult.value;
  const refGenerator = new RefGenerator(db);
  const messageEditLogRef = refGenerator.newMessageEditLog();

  // create & save doc
  const messageEditLogDoc: DocTypes.IMessageEditLogDocument = {
    bottle: messageDoc.bottle,
    message: messageRef.ref,
    text: messageDoc.text,
    timestamp,
    user: messageDoc.user
  };
  await messageEditLogRef.create(messageEditLogDoc);

  return resultType.right({
    messageEditLogDoc,
    messageEditLogRef
  });
};

export default createTag;

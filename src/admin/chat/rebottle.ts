import { either } from "fp-ts";

import { TagRef } from "../../admin/ref/pureeRef";
import * as resultType from "../../type/result";
import { Db, DocumentReference } from "../client";
import * as DocTypes from "../firestoreDocType";
import { PureeRef, RefGenerator } from "../ref";

export interface IProps {
  bottleId: string;
}

type Result = resultType.StandardResult<"not-found", null>;

export const rebottle = async (db: Db, data: IProps): Promise<Result> => {
  // ref for bottle
  const refManager = new RefGenerator(db);
  const bottleRef = refManager.bottle(data.bottleId);

  // use transaction
  return await db.runTransaction(
    async (transaction): Promise<Result> => {
      // get bottle doc
      const bottleDataResult = await bottleRef.dataInTransaction(transaction);

      if (!bottleDataResult.data) {
        return resultType.leftError("not-found");
      }

      const bottleData = bottleDataResult.data;
      if (bottleData.finished) {
        return resultType.right(null);
      }

      // create next bottle
      const nextIndex = bottleData.index! + 1;
      const nextBottleRef = refManager.newBottle();
      const nextBottleData: DocTypes.IBottleDocument = {
        index: nextIndex,
        tag: bottleData.tag
      };
      await nextBottleRef.createInTransaction(transaction, nextBottleData);

      // update tag doc
      const nextTagData: DocTypes.ITag = {
        currentBottle: nextBottleRef.ref,
        currentIndex: nextIndex,
        [`bottles.${nextIndex}`]: nextBottleRef.ref
      };

      const tagRef: TagRef = new PureeRef(bottleData.tag as DocumentReference);
      await tagRef.updateInTransaction(transaction, nextTagData);

      // update used bottle
      await bottleRef.updateInTransaction(transaction, { finished: true });
      return resultType.right(null);
    }
  );
};

export default rebottle;

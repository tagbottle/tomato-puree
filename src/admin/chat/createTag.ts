import { either } from "fp-ts";

import * as resultType from "../../type/result";
import { Firestore } from "../client";
import * as DocTypes from "../firestoreDocType";
import { PureeRef, RefGenerator } from "../ref";

import encodedTagForThemes from "../..//util/convert/tag/encodeThemesToTagString";
import { TagRef } from "../../admin/ref/pureeRef";
import isInvalidThemes from "../../util/validate/tag/isInvalidThemes";

export interface IProps {
  themes: string[];
}

export interface ISuccessPayload {
  tagRef: TagRef;
  tagData: DocTypes.ITag;
}

type Result = resultType.StandardResult<
  "not-found" | "invalid-argument" | "forbidden",
  ISuccessPayload
>;

export const createTag = async (
  db: Firestore.Firestore,
  data: IProps
): Promise<Result> => {
  // validation
  const { themes } = data;
  if (isInvalidThemes(themes)) {
    return resultType.leftError("invalid-argument");
  }
  // ref for tag
  const refManager = new RefGenerator(db);
  const tagRef = refManager.tag(themes)!;

  // start transaction
  const result = await db.runTransaction(
    async (transaction): Promise<ISuccessPayload> => {
      // get tag doc
      // if target tag already exists, just return it
      const currentTagDataResult = await tagRef.dataInTransaction(transaction);
      if (currentTagDataResult.data) {
        const currentTagData = currentTagDataResult.data;
        return { tagRef, tagData: currentTagData };
      }

      // create first bottle
      const bottleRef = refManager.newBottle();
      const bottleData: DocTypes.IBottle = {
        index: 1,
        tag: tagRef.ref
      };
      await bottleRef.setInTransaction(transaction, bottleData);

      // create tag
      const parentTag =
        themes.length !== 1
          ? encodedTagForThemes(themes.slice(0, themes.length - 1))
          : null;
      const tagData: DocTypes.ITag = {
        bottles: {
          [1]: bottleRef.ref
        },
        currentBottle: bottleRef.ref,
        currentIndex: 1,
        parent: parentTag
      };
      await tagRef.setInTransaction(transaction, tagData);

      // return result
      return { tagRef, tagData };
    }
  );

  return resultType.right(result);
};

export default createTag;

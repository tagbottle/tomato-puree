import * as DocTypes from "../../admin/firestoreDocType";
import {
  DocumentReference,
  DocumentSnapshot,
  Precondition,
  SetOptions,
  Transaction,
  WriteResult
} from "../client";

export class PureeRef<AvailableProps, DocInterface> {
  constructor(public ref: DocumentReference) {}

  /*
    set
   */

  public async set(data: AvailableProps, options: SetOptions) {
    return this.ref.set(data, options);
  }

  public async setInTransaction(
    transaction: Transaction,
    data: AvailableProps,
    options: SetOptions = {}
  ) {
    return await transaction.set(this.ref, data, options);
  }

  /*
    create
   */

  public async create(data: DocInterface) {
    return this.ref.create(data);
  }

  public async createInTransaction(
    transaction: Transaction,
    data: DocInterface
  ) {
    return await transaction.create(this.ref, data);
  }

  /*
    update
   */

  public async update(data: AvailableProps, precondition?: Precondition) {
    return this.ref.update(data, precondition);
  }

  public async updateInTransaction(
    transaction: Transaction,
    data: AvailableProps,
    precondition?: Precondition
  ) {
    return transaction.update(this.ref, data, precondition);
  }

  /*
    get data
   */

  public async data(): Promise<{
    data: DocInterface | null;
    snapshot: DocumentSnapshot;
  }> {
    // get
    const snapshot = await this.ref.get();
    if (!snapshot.exists) {
      return { data: null, snapshot };
    }

    // cast
    const data = snapshot.data() as DocInterface;
    return { data, snapshot };
  }

  public async dataInTransaction(
    transaction: Transaction
  ): Promise<{ data: DocInterface | null; snapshot: DocumentSnapshot }> {
    // get

    const snapshot = await transaction.get(this.ref);
    if (!snapshot.exists) {
      return { data: null, snapshot };
    }

    // cast
    const data = snapshot.data() as DocInterface;
    return { data, snapshot };
  }

  /*
    delete
   */

  public async delete(precondition?: Precondition): Promise<WriteResult> {
    // delete
    return await this.ref.delete(precondition);
  }

  public async deleteInTransaction(
    transaction: Transaction,
    precondition?: Precondition
  ): Promise<Transaction> {
    // delete
    return await transaction.delete(this.ref, precondition);
  }
}

/*
  message
 */
export type MessageRef = PureeRef<DocTypes.IMessage, DocTypes.IMessageDocument>;
export type MessageStampSetRef = PureeRef<
  DocTypes.IMessageStampSet,
  DocTypes.IMessageStampSetDocument
>;
export type MessageStampRequestRef = PureeRef<
  DocTypes.IMessageStampRequest,
  DocTypes.IMessageStampRequestDocument
>;
export type MessageEditLogRef = PureeRef<
  DocTypes.IMessageEditLog,
  DocTypes.IMessageEditLogDocument
>;

/*
  stamp stock
 */
export type StampStockRef = PureeRef<
  DocTypes.IStampStock,
  DocTypes.IStampStockDocument
>;
export type StampStockStampSetRef = PureeRef<
  DocTypes.IStampStockStampSet,
  DocTypes.IStampStockStampSetDocument
>;
export type StampStockReceiptRef = PureeRef<
  DocTypes.IStampStockReceipt,
  DocTypes.IStampStockReceiptDocument
>;
export type StampStockReceiptItemRef = PureeRef<
  DocTypes.IStampStockReceiptItem,
  DocTypes.IStampStockReceiptItemDocument
>;

/*
  other types
 */

export type AdminRef = PureeRef<DocTypes.IAdmin, DocTypes.IAdminDocument>;
export type BookmarkRef = PureeRef<
  DocTypes.IBookmark,
  DocTypes.IBookmarkDocument
>;
export type BottleRef = PureeRef<DocTypes.IBottle, DocTypes.IBottleDocument>;

export type StampRef = PureeRef<DocTypes.IStamp, DocTypes.IStampDocument>;
export type StampOrderRef = PureeRef<
  DocTypes.IStampOrder,
  DocTypes.IStampOrderDocument
>;
export type StampReceiptRef = PureeRef<
  DocTypes.IStampReceipt,
  DocTypes.IStampReceiptDocument
>;
export type StampSetRef = PureeRef<
  DocTypes.IStampSet,
  DocTypes.IStampSetDocument
>;
export type StampSetItemRef = PureeRef<
  DocTypes.IStampSetItem,
  DocTypes.IStampSetItemDocument
>;

export type TagRef = PureeRef<DocTypes.ITag, DocTypes.ITagDocument>;
export type UserRef = PureeRef<DocTypes.IUser, DocTypes.IUserDocument>;
export type UsernameRef = PureeRef<
  DocTypes.IUsername,
  DocTypes.IUsernameDocument
>;

export type UserDeviceRef = PureeRef<
  DocTypes.IUserDevice,
  DocTypes.IUserDeviceDocument
>;

export default PureeRef;

import * as DocTypes from "../../admin/firestoreDocType";
import { DocumentReference, DocumentSnapshot, Transaction } from "../client";
import PureeRef from "./pureeRef";

export class PureeRefList<AvailableProps, DocInterface> {
  constructor(public refs: Array<PureeRef<AvailableProps, DocInterface>>) {}

  /*
    get data
   */

  public async data(): Promise<
    Array<{ data: DocInterface | null; snapshot: DocumentSnapshot }>
  > {
    // get
    const promises = this.refs.map(async r => await r.data());
    return await Promise.all(promises);
  }

  public async dataInTransaction(
    transaction: Transaction
  ): Promise<Array<{ data: DocInterface | null; snapshot: DocumentSnapshot }>> {
    // get
    const rawRefs = this.refs.map(r => r.ref);
    const snapshots = await transaction.getAll(...rawRefs);

    // cast
    return snapshots.map(snapshot => {
      if (!snapshot.exists) {
        return { data: null, snapshot };
      }

      const data = snapshot.data() as DocInterface;
      return { data, snapshot };
    });
  }

  /*
    delete all
   */

  public async deleteAll(): Promise<number> {
    const promises = this.refs.map(r => r.delete());
    await Promise.all(promises);
    return this.refs.length;
  }

  public async deleteAllInTransaction(
    transaction: Transaction
  ): Promise<number> {
    const promises = this.refs.map(r => r.deleteInTransaction(transaction));
    await Promise.all(promises);
    return this.refs.length;
  }
}

export type StampStockReceiptRefList = PureeRefList<
  DocTypes.IStampStockReceipt,
  DocTypes.IStampStockReceiptDocument
>;

export type MessageStampRequestRefList = PureeRefList<
  DocTypes.IMessageStampRequest,
  DocTypes.IMessageStampRequestDocument
>;

export default PureeRefList;

import * as DocTypes from "../../admin/firestoreDocType";
import { Query, QuerySnapshot } from "../client";
import { PureeRef } from "./pureeRef";

export class PureeQuery<AvailableProps, DocInterface> {
  constructor(public query: Query) {}

  /*
    get all data
   */

  public async data(): Promise<{
    list: Array<{
      data: DocInterface;
      ref: PureeRef<AvailableProps, DocInterface>;
    }>;
    snapshot: QuerySnapshot;
  }> {
    // get
    const snapshot = await this.query.get();

    // cast
    const list: Array<{
      data: DocInterface;
      ref: PureeRef<AvailableProps, DocInterface>;
    }> = [];
    snapshot.forEach(result => {
      const data = result.data() as DocInterface;
      const ref = new PureeRef<AvailableProps, DocInterface>(result.ref);
      list.push({ data, ref });
    });
    return { list, snapshot };
  }
}

export type StampStockReceiptsQuery = PureeQuery<
  DocTypes.IStampStockReceipt,
  DocTypes.IStampStockReceiptDocument
>;

export type MessageStampRequestsQuery = PureeQuery<
  DocTypes.IMessageStampRequest,
  DocTypes.IMessageStampRequestDocument
>;

export type StampSetQuery = PureeQuery<
  DocTypes.IStampSet,
  DocTypes.IStampSetDocument
>;

export default PureeQuery;

import { StandardError } from "../../type/result";
import encodeThemesToTagString from "../../util/convert/tag/encodeThemesToTagString";
import { Db } from "../client";
import {
  MessageStampRequestsQuery,
  PureeQuery,
  StampStockReceiptsQuery
} from "./pureeQuery";
import {
  AdminRef,
  BookmarkRef,
  BottleRef,
  MessageEditLogRef,
  MessageRef,
  MessageStampRequestRef,
  MessageStampSetRef,
  PureeRef,
  StampReceiptRef,
  StampSetRef,
  StampStockReceiptRef,
  StampStockRef,
  StampStockStampSetRef,
  TagRef,
  UserDeviceRef,
  UserRef
} from "./pureeRef";

export class RefGenerator {
  constructor(public db: Db) {}

  /*
    admin
   */

  public admin(userId: string): AdminRef {
    // validation
    if (!userId) {
      throw new StandardError("invalid-argument", "AdminRef requires userId");
    }

    // create ref
    const ref = this.db.collection("admins").doc(userId);
    return new PureeRef(ref);
  }

  /*
    bookmark
   */

  public bookmark(userId: string): BookmarkRef {
    // validation
    if (!userId) {
      throw new StandardError(
        "invalid-argument",
        "BookmarkRef requires userId"
      );
    }

    // create ref
    const ref = this.db.collection("tags").doc(userId);
    return new PureeRef(ref);
  }

  /*
    bottle
  */

  public newBottle(): BottleRef {
    const ref = this.db.collection("bottles").doc();
    return new PureeRef(ref);
  }

  public bottle(bottleId: string): BottleRef {
    const ref = this.db.collection("bottles").doc(bottleId);
    return new PureeRef(ref);
  }

  /*
    tag
  */

  public tag(themes: string[]): TagRef {
    // validation
    if (!themes.length) {
      throw new StandardError(
        "invalid-argument",
        "TagRef requires themes.length"
      );
    }
    const encodedTag = encodeThemesToTagString(themes);
    if (!encodedTag) {
      throw new StandardError(
        "invalid-argument",
        "TagRef require correct encodedTag"
      );
    }

    // create ref
    const ref = this.db.collection("tags").doc(encodedTag);
    return new PureeRef(ref);
  }

  /*
    message
   */

  public newMessage(): MessageRef {
    // create ref
    const ref = this.db.collection("messages").doc();
    return new PureeRef(ref);
  }
  public message(messageId: string): MessageRef {
    // create ref
    const ref = this.db.collection("messages").doc(messageId);
    return new PureeRef(ref);
  }

  public newMessageEditLog(): MessageEditLogRef {
    // create ref
    const ref = this.db.collection("messageEditLogs").doc();
    return new PureeRef(ref);
  }

  public messageEditLog(messageId: string): MessageEditLogRef {
    // create ref
    const ref = this.db.collection("messageEditLogs").doc(messageId);
    return new PureeRef(ref);
  }

  /*
    user
  */

  public user(userId: string): UserRef {
    // validation
    if (!userId) {
      throw new StandardError("invalid-argument", "UserRef requires userId");
    }

    // create ref
    const ref = this.db.collection("users").doc(userId);
    return new PureeRef(ref);
  }

  public userDevice(userId: string): UserDeviceRef {
    // validation
    if (!userId) {
      throw new StandardError(
        "invalid-argument",
        "UserDeviceRef requires userId"
      );
    }

    // create ref
    const ref = this.db.collection("userDevices").doc(userId);
    return new PureeRef(ref);
  }

  /*
    stamp set
  */

  public newStampSet(): StampSetRef {
    // create ref
    const ref = this.db.collection("stampSets").doc();
    return new PureeRef(ref);
  }

  public stampSet(stampSetId: string): StampSetRef {
    // create ref
    const ref = this.db.collection("stampSets").doc(stampSetId);
    return new PureeRef(ref);
  }

  /*
    stamp receipt
  */

  public newStampReceipt(): StampReceiptRef {
    // create ref
    const ref = this.db.collection("stampReceipts").doc();
    return new PureeRef(ref);
  }

  /*
    stamp stock
  */
  public stampStock(userId: string): StampStockRef {
    // create ref
    const ref = this.db.collection("stampStocks").doc(userId);
    return new PureeRef(ref);
  }

  /*
    stamp stock / stamp set
  */
  public stampStockStampSet(
    userId: string,
    stampSetRefId: string
  ): StampStockStampSetRef {
    // create ref
    const stampStockRef = this.stampStock(userId);
    const ref = stampStockRef.ref.collection("stampSets").doc(stampSetRefId);
    return new PureeRef(ref);
  }

  /*
    stamp stock / stamp set / receipt
  */
  public stampStockReceipt(
    userId: string,
    stampSetRefId: string,
    stampReceiptRefId: string
  ): StampStockReceiptRef {
    // create ref
    const stampStockStampSetRef = this.stampStockStampSet(
      userId,
      stampSetRefId
    );
    const ref = stampStockStampSetRef.ref
      .collection("receipts")
      .doc(stampReceiptRefId);
    return new PureeRef(ref);
  }

  public stampStockReceipts(
    userId: string,
    stampSetRefId: string
  ): StampStockReceiptsQuery {
    // create ref
    const stampStockStampSetRef = this.stampStockStampSet(
      userId,
      stampSetRefId
    );
    const query = stampStockStampSetRef.ref
      .collection("receipts")
      .where("amount", ">", 0)
      .orderBy("amount", "desc")
      .limit(1);

    return new PureeQuery(query);
  }

  /*
    stamp of message
   */

  public messageStampSet(
    messageId: string,
    stampSetId: string
  ): MessageStampSetRef {
    // create ref
    const ref = this.db
      .collection("messages")
      .doc(messageId)
      .collection("stampSets")
      .doc(stampSetId);
    return new PureeRef(ref);
  }

  public newMessageStampRequest(
    messageId: string,
    stampSetId: string
  ): MessageStampRequestRef {
    // create ref
    const messageStampSetRef = this.messageStampSet(messageId, stampSetId);
    const ref = messageStampSetRef.ref.collection("requests").doc();
    return new PureeRef(ref);
  }

  public messageStampRequests(
    messageId: string,
    stampSetId: string
  ): MessageStampRequestsQuery {
    // create ref
    const messageStampSetRef = this.messageStampSet(messageId, stampSetId);
    const ref = messageStampSetRef.ref.collection("requests");
    return new PureeQuery(ref);
  }
}

export default RefGenerator;

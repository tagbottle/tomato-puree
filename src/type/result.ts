import { either } from "fp-ts";

// error

export type ErrorCode =
  // firebase functions's http error code
  | "ok"
  | "cancelled"
  | "unknown"
  | "invalid-argument"
  | "deadline-exceeded"
  | "not-found"
  | "already-exists"
  | "permission-denied"
  | "resource-exhausted"
  | "failed-precondition"
  | "aborted"
  | "out-of-range"
  | "unimplemented"
  | "internal"
  | "unavailable"
  | "data-loss"
  | "unauthenticated"

  // added by puree
  | "runtime-exception"
  | "forbidden";

export interface IError<SuccessPayload> {
  code: ErrorCode;
  message: string;
  payload: SuccessPayload;
}
export class StandardError<E extends ErrorCode> implements IError<undefined> {
  public payload: undefined;
  constructor(public code: E, public message: string = "") {
    if (message === "") {
      this.message = code;
    }
  }
}

// result

export type PureeResult<ErrorPayload, SuccessPayload> = either.Either<
  IError<ErrorPayload>,
  SuccessPayload
>;
export type StandardResult<E extends ErrorCode, R> = either.Either<
  StandardError<E>,
  R
>;

// helper
export const right = either.right;
export const leftError = <E extends ErrorCode, R>(
  errorCode: E,
  message?: string
): StandardResult<E, R> => {
  const error = new StandardError(errorCode, message);
  return either.left(error);
};

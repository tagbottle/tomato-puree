# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased](https://gitlab.com/tagbottle/tomato-puree/compare/v5.4.0...HEAD) 

## [5.4.0](https://gitlab.com/tagbottle/tomato-puree/compare/v5.3.1...v5.4.0) - 2018-08-23
### Added
- `citations` to message interfaces
- license clause in package.json

## [5.3.1](https://gitlab.com/tagbottle/tomato-puree/compare/v5.3.0...v5.3.1) - 2018-08-20
### Added
- more reliable validation in ping logic 
### Fixed
- invalid path for user-device doc

## [5.3.0](https://gitlab.com/tagbottle/tomato-puree/compare/v5.2.1...v5.2.2) - 2018-08-20
### Added
- client's ref generator method for user-device doc 


## [5.2.1](https://gitlab.com/tagbottle/tomato-puree/compare/v5.2.0...v5.2.1) - 2018-08-20
### Changed
- each edit logs of one message use unique path 


## [5.2.0](https://gitlab.com/tagbottle/tomato-puree/compare/v5.1.0...v5.2.0) - 2018-08-20
### Added
- Firestore doc for user devices
- `ping` logic


## [5.1.0](https://gitlab.com/tagbottle/tomato-puree/compare/v5.0.0...v5.1.0) - 2018-08-20
### Added
- Firestore doc for message edit logs


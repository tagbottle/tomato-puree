import "jest";

import encode from "@/util/convert/messaging/encode";

describe("encode", () => {
  it("returns encoded string", () => {
    const input = "tagbottle/bottole.debug(!)";
    const output = "tagbottle%2Fbottole.debug%28%21%29";
    expect(encode(input)).toEqual(output);
  });
  it("returns different output if input is already escaped", () => {
    const input = "tagbottle/bottole.debug%28%21%29";
    const output = "tagbottle%2Fbottole.debug%2528%2521%2529";
    expect(encode(input)).toEqual(output);
  });
});

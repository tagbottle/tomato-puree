import "jest";

import tagSubscribeToken from "@/util/convert/messaging/tagSubscribeToken";

describe("tagSubscribeToken", () => {
  it("returns encoded string", () => {
    const input = ["tag", "test", "a/b"];
    const output = "/topics/tag%2Ftest%2Fa%252Fb";
    expect(tagSubscribeToken(input)).toEqual(output);
  });
  it("returns double-encoded values if input contains slash in array", () => {
    const input1 = ["tag", "test", "a/b"];
    const input2 = ["tag", "test", "a", "b"];
    const output1 = tagSubscribeToken(input1);
    const output2 = tagSubscribeToken(input2);
    expect(output1).not.toEqual(output2);
  });
});

import "jest";

import nestedList from "@/util/convert/tag/nestedList";

describe("nestedList", () => {
  it("generates nested list from list of tags", () => {
    const input = [
      "tagbottle",
      "tagbottle/dev/bug-report",
      "soccer/wc2018/live",
      "bot/news",
      "tagbottle/dev/talk"
    ];
    /* tslint:disable:object-literal-sort-keys */
    const output = [
      {
        theme: "tagbottle",
        themes: ["tagbottle"],
        active: true,
        children: [
          {
            theme: "dev",
            themes: ["tagbottle", "dev"],
            active: false,
            children: [
              {
                theme: "bug-report",
                themes: ["tagbottle", "dev", "bug-report"],
                active: true,
                children: []
              },
              {
                theme: "talk",
                themes: ["tagbottle", "dev", "talk"],
                active: true,
                children: []
              }
            ]
          }
        ]
      },
      {
        theme: "soccer",
        themes: ["soccer"],
        active: false,
        children: [
          {
            theme: "wc2018",
            themes: ["soccer", "wc2018"],
            active: false,
            children: [
              {
                theme: "live",
                themes: ["soccer", "wc2018", "live"],
                active: true,
                children: []
              }
            ]
          }
        ]
      },
      {
        theme: "bot",
        themes: ["bot"],
        active: false,
        children: [
          {
            theme: "news",
            themes: ["bot", "news"],
            active: true,
            children: []
          }
        ]
      }
    ];
    /* tslint:enable:object-literal-sort-keys */
    expect(nestedList(input)).toEqual(output);
  });
});

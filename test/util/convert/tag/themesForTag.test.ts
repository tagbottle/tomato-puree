import "jest";

import themesForTag from "@/util/convert/tag/themesForTag";

describe("themesForTag", () => {
  it("split tag", () => {
    const input = "a/b c/d";
    const output = ["a", "b c", "d"];
    expect(themesForTag(input)).toEqual(output);
  });
});

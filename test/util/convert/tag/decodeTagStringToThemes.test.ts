import "jest";

import decodeTagStringToThemes from "@/util/convert/tag/decodeTagStringToThemes";

describe("encodeThemesToTagString", () => {
  it("encodes and joins themes", () => {
    const input = "a%2Fb%2Fhttps%253A%252F%252Fexample%09%252Ecom%252F";
    const output = ["a", "b", "https://example.com/"];
    expect(decodeTagStringToThemes(input)).toEqual(output);
  });
});

import "jest";

import encodeThemesToTagString from "@/util/convert/tag/encodeThemesToTagString";

describe("encodeThemesToTagString", () => {
  it("encodes and joins themes", () => {
    const input = ["a", "b", "https://example.com/"];
    const output = "a%2Fb%2Fhttps%253A%252F%252Fexample%09%252Ecom%252F";
    expect(encodeThemesToTagString(input)).toEqual(output);
  });
  it("creates different outputs from similar inputs", () => {
    const input1 = ["https://example.com/a"];
    const input2 = ["https://example.com", "a"];
    const output1 = encodeThemesToTagString(input1);
    const output2 = encodeThemesToTagString(input2);
    expect(output1).not.toEqual(output2);
  });
});

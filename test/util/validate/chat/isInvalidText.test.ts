import "jest";

import isInvalidText from "@/util/validate/chat/isInvalidText";

describe("isInvalidText", () => {
  it("returns error if text is null", () => {
    const input = null;
    const output = "EMPTY";
    expect(isInvalidText(input)).toEqual(output);
  });
  it("returns error if text is empty", () => {
    const input = "";
    const output = "EMPTY";
    expect(isInvalidText(input)).toEqual(output);
  });
});

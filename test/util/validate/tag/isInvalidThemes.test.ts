import "jest";

import isInvalidThemes from "@/util/validate/tag/isInvalidThemes";

describe("isInvalidThemes", () => {
  it("returns error if themes is null", () => {
    const input = null;
    const output = "NOT_ARRAY";
    expect(isInvalidThemes(input)).toEqual(output);
  });
  it("returns error if themes is empty", () => {
    const input = [];
    const output = "EMPTY";
    expect(isInvalidThemes(input)).toEqual(output);
  });
  it("returns error if themes contains blank", () => {
    const input = ["a", "b", "", "d"];
    const output = "INVALID_THEME_INCLUDED";
    expect(isInvalidThemes(input)).toEqual(output);
  });
});

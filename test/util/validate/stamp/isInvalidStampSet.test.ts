import "jest";

import { IStampSetDocument } from "@/common/firestoreDocType/stampSet";
import isInvalidStampSet from "@/util/validate/stamp/isInvalidStampSet";

describe("isInvalidStampSet", () => {
  it("returns false if prop is a correct stampSet document", () => {
    const target: IStampSetDocument = {
      items: [
        {
          id: "test stamp item id",
          index: 1,
          name: "test stamp item name",
          url: "https://test.url/image.jpg"
        }
      ],
      title: "test title",
      vendor: "test vendor"
    };
    expect(isInvalidStampSet(target)).toEqual(false);
  });
});
